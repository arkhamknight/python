# pyhton excercise 18
# types of functions

def print_none():
    print("hello inside print1")

def print_one(arg1):
    print("inside print_one function arg1 :-  ", arg1)

# this function can take n no of arguments
def print_many(*args):
    print("length of args :- ",len(args))
    print("printing elements of an array :-")
    for var in args:
        print(var)

# default arguments function

def print_def(two, one = 10):
    print("one :- ", one)
    print("two :- ", two)

print_none()
print_one(1)
print_many("1",1,3,4,4)

# calling default values function
print_def(2)
print_def(11,22)
